from setuptools import setup

setup(
    name = "icingaparse",
    version = "0.0.3",
    author = "Greg Trahair",
    author_email = "greg.trahair@gmail.com",
    description = 'Icinga Object Cache Parser',
    license = "Apache",
    keywords = "utils",
    url = 'https://bitbucket.org/geogdog/icingaparse',
    py_modules = ['icingaparse'],
    long_description = 'Icinga Object Cache Parser.',
    classifiers = [
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: Apache Software License",
    ],
)
