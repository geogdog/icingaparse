#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# icingaparse.py
# Copyright 2012 Greg Trahair <greg.trahair@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import re
import json
import yaml


def parseConf(filename):
    conf = []
    with open(filename, 'r') as f:
        for i in f:
            if i[0] == '#':
                continue
            matchID = re.search(r"([\w]+) {", i)
            matchAttr = re.search(r"\s+([\w]+)\s([\w\d]*)", i)
            matchEndID = re.search(r"[ ]*}", i)
            if matchID:
                identifier = matchID.group(1)
                cur = [identifier, {}]
            elif matchAttr:
                attribute = matchAttr.group(1)
                value = matchAttr.group(2)
                cur[1][attribute] = value
            elif matchEndID:
                conf.append(cur)
    return conf


def conf2xml(filename):
    conf = parseConf(filename)
    xml = ''
    xmlfmtstr = '\t<attribute name="%s">%s</attribute>\n'
    for ID in conf:
        xml += '<%s>\n' % ID[0]
        for attr in ID[1]:
            xml += xmlfmtstr % (attr, ID[1][attr])
        xml += '</%s>\n' % ID[0]
    return xml


def conf2json(filename):
    conf = parseConf(filename)
    return json.dumps(conf, indent=2)


def conf2yaml(filename):
    conf = parseConf(filename)
    return yaml.safe_dump(conf, default_flow_style=False, indent=2)


if __name__ == '__main__':
    filename = 'testdata/objects.cache'
    print conf2xml(filename)
    print conf2json(filename)
    print conf2yaml(filename)
